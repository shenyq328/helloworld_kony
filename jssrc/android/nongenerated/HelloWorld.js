/**
 ****************************************************************
 *	Name    : showPopup
 *	Author  : Kony Inc.
 *	Purpose : Displays the popup
 ****************************************************************
 */
function showPopup() {
    var dialogMessage = kony.i18n.getLocalizedString("hello");;
    kony.ui.Alert({
        message: dialogMessage,
        alertType: constants.ALERT_TYPE_INFO,
        yesLabel: "Close"
    }, {}); //usage: kony.ui.Alert(basicProperties,layoutProperties,platformSpecificProperties); 
    return;
}

function exit() {
    try {
        kony.application.exit();
        //frmHome.
    } catch (Error) {
        alert("Exception While getting exiting the application  : " + Error);
    }
}
/*var hasWorkerSupport = kony.worker.hasWorkerThreadSupport();

if (hasWorkerSupport) {
	kony.print("<<<<<<<<workers supported>>>>>>>>>");
    //create new worker
	var worker = new kony.worker.WorkerThread('WorkerThread.js');
    //do something
	//invoked when worker calls postmessage() from its inner scope
	worker.addEventListener("message", function (event) {
	    kony.print('Parent Scope : onmessage : event.data : ' + event.data["message"]);
	});
	
	kony.print('Parent Scope : Invoking worker.postmessage()');
//will invoke worker's inner scope onmessage()
	/*worker.postMessage({
	    'message': 'Hello World From Parent'
	});*/
/*
} else {
    //workers not supported
	kony.print("<<<<<<<workers not supported>>>>>>>>>");
}*/
function handleAppBehaviour() {
    kony.print("Inside handleAppBehaviour");
    var callbacksObj = {
        onbackground: appGoesOnBackGround,
        onforeground: appComesOnForeGround
    };
    kony.application.setApplicationCallbacks(callbacksObj);
}

function appGoesOnBackGround() {
    alert("Inside appGoesOnBackGround... current Locale is : " + kony.i18n.getCurrentLocale() + " and device locale is : " + kony.i18n.getCurrentDeviceLocale());
    var localeValue = kony.i18n.getCurrentDeviceLocale();
    kony.i18n.setCurrentLocaleAsync(localeValue, localeSuccessCallback, localeFailureCallback, null);
}

function appComesOnForeGround() {
    alert("Inside appComesOnForeGround... current Locale is : " + kony.i18n.getCurrentLocale() + " and device locale is : " + kony.i18n.getCurrentDeviceLocale());
    kony.i18n.setCurrentLocaleAsync(localeValue, localeSuccessCallback, localeFailureCallback, null);
}

function localeSuccessCallback() {
    frmHome.show();
}

function localeFailureCallback() {
    alert("Exception occured while changing the locale");
}